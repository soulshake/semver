# semver-pipe

This repository contains a reusable pipe to facilitate semantic versioning in CI/CD on Bitbucket Pipelines.

## Usage

In the source repository, include a step like the following:

```
pipelines:
  custom:                                                 # `custom` indicates a pipeline that can only be triggered manually
    release:
      - step:
          name: "Release"
          script:
            - pipe: soulshake/semver:main
              variables:
                BITBUCKET_USERNAME: 'Anonymous'           # defaults to `bitbucket-pipelines`
                BITBUCKET_EMAIL: 'anonymous@example.com'  # defaults to `<commits-noreply@bitbucket.org>`
                UPDATE_FILES: 'README.md other-file.yml'  # defaults to `README.md`
```

Because this pipe automatically pushes tags and commits back to the source repository, be sure to use a `custom` pipeline (which can only be triggered manually) to avoid creating an infinite loop.

## Variables

### Git author info

If set, the following arguments will be used to set the author information on the commits pushed back to git:

- `BITBUCKET_USERNAME`
- `BITBUCKET_EMAIL`

### Keep certain files in sync with the latest version

If certain files in the repo should always contain a reference to the latest release, set `UPDATE_FILES` to a space-separated list of filenames.

The pipe will replace references to the previous version with the new version in each of the specified files.

This defaults to `README.md`.

## Workflow

Note, it is assumed that:

- the source repository's `bitbucket-pipelines.yml` is configured as described in the "Usage" section above.
- developers have installed `semversioner` locally (`pip install semversioner`).

This pipe is expected to fit into a workflow like the following:

1. A developer works on a branch, using `semversioner add-change` as needed and committing the resulting files.

2. The branch is merged to trunk via pull request.

3. When it's time to release the changes, the developer manually runs the `release` pipeline, which triggers this pipe, which has the following effects:

- This pipe runs `semversioner release`, which generates the relevant files in the `.semversioner` directory in the source repo.
- `CHANGELOG.md` is updated with the new release details.
- Any files in `${UPDATE_FILES}` are updated to replace references to the previous version with the new one.
- The pipe pushes these changes in a new commit back to the source repo.
- The pipe pushes a new git tag (with the same name as the new version).
