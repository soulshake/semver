FROM python:3.9

RUN pip install \
  semversioner==1.0.0

WORKDIR /src
COPY . .
CMD ["/src/entrypoint.sh"]
