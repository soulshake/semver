#!/usr/bin/env bash

set -e

setup_git() {
    local -

    [ -f /.dockerenv ] || {
        log_warning "Only do this in docker"
        return 1
    }

    set -x
    # https://community.atlassian.com/t5/Bitbucket-questions/Bitbucket-Pipe-push-back-to-repo/qaq-p/1096417
    git config "http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy" http://host.docker.internal:29418/
    [ -n "${BITBUCKET_EMAIL:-}" ] && git config --global user.email "${BITBUCKET_EMAIL}"
    [ -n "${BITBUCKET_USERNAME:-}" ] && git config --global user.name "${BITBUCKET_USERNAME}"
}

needs_release() {
    # Check for changelog files.
    local result
    result=$(semversioner status)
    if [[ "${result}" == *"No changes to release"* ]]; then
        echo "${result}"
        return 1
    fi

    if [[ "${result}" != *"Unreleased changes:"* ]]; then
        echo "Got weird output from 'semversioner status':"
        echo "${result}"
        return 1
    fi

    echo "Found some stuff to release!"
    echo "${result}"
}

ensure_version_changelog() {
    # If no version changes have been staged, we can either generate them (if this is a
    # merge to main), or fail the pipeline (to require the developer to commit changelog
    # updates).
    if ! needs_release; then
        case "${BITBUCKET_BRANCH}" in
        main | master)
            # This means the developer never ran `semversioner add-change`. We can just do it for them using their commit messages.
            messages=$(git rev-list --format=%B "$(git show -s --format=%H)" --max-count=1 | grep -v '^commit ' | grep -v "^$")
            semversioner add-change -t patch -d "${messages}"
            git add .
            git commit -m "Generate semversioner change from commit messages."
            ;;
        *)
            # In this case we need to make sure the branch includes version updates for its changes.
            echo "Error: No version bump detected."
            # shellcheck disable=SC2016
            echo 'Please run `semversioner add-change`.'
            return 1
            ;;
        esac
    fi
}

ensure_clean() {
    if [ -n "$(git status --porcelain)" ]; then
        echo "Can't proceed due to unexpected local changes."
        git status
        return 1
    fi
    echo "Git is clean!"
}

do_release() {
    ##
    # Generate new version; update CHANGELOG.md and any files in ${UPDATE_FILES}.
    ##
    local cur_version new_version

    # Update the release files.
    cur_version=$(semversioner current-version)
    semversioner release
    new_version=$(semversioner current-version)

    # Generate CHANGELOG.md.
    echo "Generating CHANGELOG.md file..."
    semversioner changelog >CHANGELOG.md
    echo

    # Replace references to previous version in values specified in ${UPDATE_FILES}.
    for f in ${UPDATE_FILES}; do
        [ ! -f "${f}" ] && echo "Can't update version in file '${f}' as it does not exist; skipping" && continue

        echo "Replace version '${cur_version}' to '${new_version}' in ${f}..."
        sed -i "s/${BITBUCKET_REPO_SLUG}:[0-9]*\.[0-9]*\.[0-9]*/${BITBUCKET_REPO_SLUG}:${new_version}/g" "${f}"
    done
}

commit_tag_and_push() {
    local new_version="${1?}"
    ##
    # Commit release files, push back to the repository, and tag the release.
    ##
    echo "Committing updated files to the repository..."
    git add .
    git commit -m "Release version ${new_version}"
    git show
    git push origin "${BITBUCKET_BRANCH?}"
    echo

    echo "Tagging release ${new_version}"
    git tag -a -m "Tagging release ${new_version?}" "${new_version}"
    git push origin "${new_version}"
    echo
}

main() {
    : "${BITBUCKET_BRANCH?}"

    setup_git
    ensure_version_changelog || exit $?
    ensure_clean || exit $?
    do_release || exit $?
    commit_tag_and_push "$(semversioner current-version)"

    echo "For details about the semver pipe, see: https://bitbucket.org/soulshake/semver"
}

main
